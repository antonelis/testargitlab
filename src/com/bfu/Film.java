package com.bfu;

public class Film {
    private String name;
    private int age;
    private int price;

    public Film(String name, int age, int price) {

        this.name = name;
        this.age = age;
        this.price = price;
    }

    public String getName() { return name; }
    public int getPrice() {
        return price;
    }
    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setPrice(int price) {
        this.price = price;
    }

}

