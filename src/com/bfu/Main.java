package com.bfu;

import java.util.Hashtable;
import java.util.Scanner;

public class Main {

    private static String movieName;

    public static void main(String[] args) {
        Scanner scannerObject = new Scanner(System.in);
        Film filmObject1 = new Film("Spiderman", 7, 100);
        Film filmObject2 = new Film("Superman", 11, 100);
        Film filmObject3 = new Film("Batman", 15, 100);

        Hashtable<String, Film> movies = new Hashtable<String, Film>();


        movies.put("Spiderman", filmObject1);
        movies.put("Superman", filmObject2);
        movies.put("Batman", filmObject3);
        Film m = movies.get(setMovieName());
        System.out.println("Ange ditt ålder, tack");

        int age = Integer.parseInt(scannerObject.nextLine());

        if (m.getAge() > age) {
            System.out.println("Vänta tills du fyller " + m.getAge() + " år");
            System.exit(0);
        }

        System.out.println("Hur många biljetter önskas?");
        int numTickets = Integer.parseInt(scannerObject.nextLine());

        int total = calcTotal(numTickets, m);

        System.out.println("Du har valt " + m.getName() + ", kostnad/biljett " + m.getPrice() + ", Betala " + total + " ");

    }

    public static int calcTotal(int numTickets, Film movie) {

        int total = 0;
        total = numTickets * movie.getPrice();

        return total;

    }
    public static String setMovieName() {
        Scanner scannerObject = new Scanner(System.in);

        System.out.println("Ange vilken film du vill se : Spiderman,Superman,Batman");

        String name = scannerObject.nextLine();

        if (!name.equals("Spiderman") && !name.equals("Superman") && !name.equals("Batman")) {
            System.out.println("Tyvärr, vi har har inte den filmen. Försök igen");
            return (setMovieName());
        }

        return name;
    }
}